# Président Yéhé-ouhouh
## Présentation
Le président (aussi appelé le troufion) est un jeu de cartes rapide et amusant, au cours duquel la hiérarchie des joueurs changera à chaque manche. Le vainqueur d'une manche devient le président, alors que le perdant est proclamé troufion. Une fois que vous maitriserez les règles de base, vous pourrez essayer différentes variantes de ce jeu très populaire.

## Règle du jeu
- Ce jeu se joue de 3 à 6 joueurs.
- Lors du premier tour, le joueur possédant la dame de coeur commence.
- L'ensemble des cartes sont distribuées aux joueurs de la manière la plus homogène.
- Ce jeu se joue par tours. Tant que quelqu'un peut et veut jouer, le tour continue et tourne dans le sens horaire.
- Le premier joueur choisit des cartes d'une même valeur et les pose sur la tables.
- Suite à celà, chaque joueur doit fournir autant de cartes que le joueur précédent des cartes dun' valeur supérieure ou égale.
- Un joueur a le droit de sauter son tour et reprendre le tour d'après.
- Un tour est fini lorsque plus personne ne joue. C'est alors le dernier à avoir joué qui ouvre la manche suivante. Ou si un joueur pose un ou plusieurs deux. Ce qui mets immédiatement fin au tour.
- L'objectif est d'être le premier à ne plus avoir de cartes. Ce joueur est alors déclaré président de la manche.
- Les joueurs restants continuent à jouer jusqu'à ce qu'il n'y ait plus qu'une joueur qui ait des cartes en main, il est alors déclaré 'troufion'

On décide alors ou non de jouer une nouvelle manche. Ce sera le troufion qui ouvrira la partie.

## Procédure

Tout d'abord, lancer le fichier main.

Ensuite sélectionner le nombre de joueurs,
la console va alors afficher le nom de chaque joueur.

Le jeu va alors lancer un premier tour, c'est le joueur qui possède la dame de cœur qui commence.

On demande alors au joueur si celui-ci veux ou non se coucher (il doit alors taper 1 pour se faire.)

Si celui-ci ne s'est pas couché alors on affiche les cartes de sa main et on lui demande quelle carte il désire jouer.

En copiant-collant la carte dans le terminal, on peut ainsi entrer la carte à jouer.

Ensuite, la dernière carte du tas est affiché et on recommence le même procédé pour le joueur suivant
et ainsi de suite jusqu'à ce que tout le monde, ce soit couché mis à part un des joueurs.

Celui-ci sera alors le premier a joué le plis suivant et ainsi de suite jusqu'à ce que tous les joueurs (mis à part un)
n'aie plus de carte dans leurs mains.

Cela marquera la fin d'un tour, la hiérarchie sera alors attribué en fonction de l'ordre dans lequel les joueurs ont
terminé leur partie, en premier le président et en dernier le trouduc

On se verra alors proposé de recommencer un tour avec les mêmes joueurs et leur hiérarchie impliquant ainsi l'échange de
carte entre le président et le trouduc.

A la fin d'un tour, enfin, on peut choisir de s'arrêter cela mettra fin à la partie avec les joueurs en cour.

## Erratum

Pour l'instant, on ne peut jouer qu'une carte l'implémentation du jeu en pair ou triplette n'est pas encore faite.

De plus, tous les joueurs sont des joueurs, les joueurs IA ne sont pas encore implémentés non plus.

Mis à part cela le jeu est fonctionnel

## Explication sur le dev

Tout est dirigé depuis la méthode main tout d'abord, on instancie un président game qui va prendre en paramètre le nombre
de joueur et afficher leur nom. On appel ensuite la methode Turn qui prend en parmêtre une liste de joueur ainsi qu'un
booléen qui détermine si c'est le premier tour ou non.

À la fin du tour, on demande si l'on désire s'arrêter ou non, si non on réitère le tour et la demande d'arrêt jusqu'à ce
que le joueur tape exit.

Pour ce qui est du tour ; lors de l'initialisation du tour on shuffle notre deck et on distribue les cartes à chaques
joueur pour le tour. Si ce n'est pas le premier tour, on effectue l'échange de carte entre le président et le trouduc,
sinon et pour la suite, on commence à joueur un pli(trick) et ce jusqu'a ce que tous les joueurs sauf un ai fini de jouer
soit n'ont plus de cartes dans les mains

Dans la méthode trick, on attribue un joueur qui commence à joueur (Si c'est le premier tour, c'est celui qui a la dame de
coeur (méthode first_turn) Sinon, c'est le joueur qui à l'attribut hiérarchie à la valeur trouduc qui commence).
On initialise un compteur qui s'incrémente à chaque fois qu'un joueur se couche, ainsi tant que tout les joeurs sauf un
ne se sont pas couché, on effectue la même manœuvre soit pour chaque joueur on appelle la méthode play qui demande quel
carte, il désire jouer, la retirer de sa main et l'ajouter au tas.