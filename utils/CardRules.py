"""
Utils object for Card

    CARD_NUMBERS : Dictonary -> number and value of all cards
    COLORS : Tuple -> types of colors of cards
"""
CARD_NUMBERS = {
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 10,
    'J': 11,
    'Q': 12,
    'K': 13,
    'A': 14,
    '2': 15
}

COLORS = ('♡', '♢', '♤', '♧')
