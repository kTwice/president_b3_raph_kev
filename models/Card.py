from president_b3_raph_kev.utils import CardRules


class Card:
    """
    Attributes
    ----------
    number : str
        number of faces who represented value of cards
    color : str
        color of cards

    Methods
    -------
    all magik methode to define how compare cards which each others
    """

    def __init__(self, number: str, color: str):
        """
        Constructs all necessary attributes for the Card object

        :param: number: str
        :param: color: str
        """
        self.number = number
        self.color = color

    def __eq__(self, other):
        return self.number == other.number

    def __gt__(self, other):
        return CardRules.CARD_NUMBERS[self.number] > CardRules.CARD_NUMBERS[other.number]

    def __ge__(self, other):
        return CardRules.CARD_NUMBERS[self.number] >= CardRules.CARD_NUMBERS[other.number]

    def __lt__(self, other):
        return CardRules.CARD_NUMBERS[self.number] < CardRules.CARD_NUMBERS[other.number]

    def __le__(self, other):
        return CardRules.CARD_NUMBERS[self.number] <= CardRules.CARD_NUMBERS[other.number]

    def __repr__(self):
        return "({number}: {color})".format(number=self.number, color=self.color)
