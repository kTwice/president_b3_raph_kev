import random
from president_b3_raph_kev.models.Card import Card
from president_b3_raph_kev.utils import CardRules


class Deck:
    """
    Attributes
    ----------
    cards : list[Card]
        all 52 cards (13 values x 4 colors)

    Methods
    -------
    shuffle()
        to shuffled cards of the deck
    """
    def __init__(self):
        cards: list[Card] = []
        i = 0
        my_keys = list(CardRules.CARD_NUMBERS.keys())
        my_value = list(CardRules.CARD_NUMBERS.values())
        for color in CardRules.COLORS:
            n = 2
            while n < 15:
                i += 1
                n += 1
                card = Card(my_keys[my_value.index(n)], color)
                cards.append(card)

        self.cards = cards

    def shuffle(self):
        """
        Randomize the deck

        :return: Deck
            with cards shuffled
        """
        random.shuffle(self.cards)
