from president_b3_raph_kev.models.Card import Card
from president_b3_raph_kev.models.Deck import Deck
from president_b3_raph_kev.models.Player import Player
from president_b3_raph_kev.models.PresidentGame import PresidentGame


class Turn(PresidentGame):
    """
    Attributes
    ----------
    number_of_player : int
        represented the number of player for this game
    players : list[Player]
        list of all player who play for one turn (until the hierarchy is attribuate)
    first_turn_bool : bool
        variable to know if it's the first turn of the game or not

    Methods
    -------
    gamers_name()
    end_of_the_game()
    """

    def __init__(self, number_of_player: int, players: list[Player], first_turn_bool: bool):
        super().__init__(number_of_player)
        self.players = players
        self.first_turn_bool = first_turn_bool
        self.wanted_stop = False
        players_finished: list[Player] = []
        president_card: Card | None = None
        trouduc_card: Card | None = None
        deck = Deck()
        deck.shuffle()
        rest = len(deck.cards) % number_of_player
        drawn_card = len(deck.cards) - rest

        """draw of all card att the begin of a turn"""
        for player in players:
            for _ in range(int(drawn_card / number_of_player)):
                card = deck.cards.pop()
                player.hand.append(card)

        """if it's not the first turn exchange card between president and 'trouduc'"""
        if not first_turn_bool:
            for player in players:
                if player.hierarchy == "trouduc":
                    trouduc_card = player.trouduc_method()
                elif player.hierarchy == "president":
                     president_card = player.president_method()
                else:
                    # gestion vice trouduc et vice président
                    continue

                if player.hierarchy == "trouduc":
                    player.add_to_hand(president_card)
                elif player.hierarchy == "president":
                    player.add_to_hand(trouduc_card)
                else:
                    # gestion vice trouduc et vice président
                    continue

        """check if the player have finnish or not then run a new trick if not"""
        while len(players) - len(players_finished) != 1:
            self.tricks(self.players)
            for player in players:
                if len(player.hand) == 0:
                    players_finished.append(player)

        """attribute hierarchy at the end of a turn"""
        players_finished[0].hierarchy = "president"
        players_finished[len(players)-1].hierarchy = "trouduc"
        if len(players) >= 4:
            players_finished[1].hierarchy = "vice_president"
            players_finished[len(players)-2].hierarchy = "vice_trouduc"

        for player in players:
            player.hierarchy = "neutre" if player.hierarchy == "" else player.hierarchy


    def first_turn(self):
        """
        methode who search which player had the heart Queen to begun

        :return: Player
            the player who will begin to play for the first turn
        """
        first_to_play: Player | None
        for player in self.players:
            player.hierarchy = "neutre"

        def find_queen(card_to_test: Card):
            """factorize of code"""
            for playerP in self.players:
                for card in playerP.hand:
                    if card == card_to_test:
                        return playerP

        first_to_play = find_queen(Card('Q', '♡'))
        if first_to_play is None:
            """For if heart queen is in discard"""
            first_to_play = find_queen(Card('Q', '♢'))
        elif first_to_play is None:
            first_to_play = find_queen(Card('Q', '♤'))
        elif first_to_play is None:
            first_to_play = find_queen(Card('Q', '♧'))

        return first_to_play

    def tricks(self, players: list[Player]):
        """
        rules for a trick in one turn you have many tricks, until remain only one player with hand

        :param: players : List of PLayer
        :return: None
        """
        the_bunch: list[Card] = []
        counter: int = 0
        beginner: Player | None = None
        self.players = players
        """check if one player is the 'trouduc' if it is he begin"""
        for player in players:
            if player.hierarchy == "trouduc":
                beginner = player

        """if there is none 'trouduc' player then run first_turn function"""
        if beginner is None:
            beginner = self.first_turn()

        while counter != len(players) - 1:
            """counter represented number of player who's stop"""
            card_played = beginner.play()
            if beginner.finished:
                counter += 1
            else:
                beginner.remove_from_hand(card_played)
                the_bunch.append(card_played)
                print("le tas : (", card_played.number, ": ", card_played.color, ")")

            for player in players:
                if player != beginner:
                    card_played_then = player.play()
                    if player.finished:
                        counter += 1
                    else:
                        """we check if the player had played a card greater or equal to the last card played"""
                        while card_played_then < next(reversed(the_bunch)):
                            print("veuillez jouer une carte plus grande ou égale à la carte précédente")
                            card_played_then = player.play()

                        player.remove_from_hand(card_played_then)
                        the_bunch.append(card_played_then)
                        print("le tas : {", card_played_then.number, ": ", card_played_then.color, "}")

        #for player in players:
            #if not player.finished:
                #player.hierarchy = "trouduc"