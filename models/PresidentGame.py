from president_b3_raph_kev.models.Deck import Deck
from president_b3_raph_kev.models.Player import Player


class PresidentGame:
    """
    Attributes
    ----------
    number_of_player : int
        represented the number of player for this game

    Methods
    -------
    gamers_name()
    end_of_the_game()
    """
    def __init__(self, number_of_player: int):
        self.number_of_player = number_of_player
        self.players: list[Player] = []
        for _ in range(number_of_player):
            self.players.append(Player())


    def gamers_name(self):
        '''
        Print all players name of the game

        :return: None
        '''
        for player in self.players:
            print("joueur : ", player.name)

    def end_of_the_game(self):
        """
        Print 2 sentances who's asked player if he/she wants to stop or continue at the end of the turn

        :return: None
        """
        print("fin du tour")
        print("pour continuer appuyez sur entrée, sinon tapez [exit]")

