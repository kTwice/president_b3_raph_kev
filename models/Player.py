import names
from president_b3_raph_kev.models.Card import Card


class Player:
    """
    Attributes
    ----------
    args : []
        name: str
            name of the player
        hand: list[Card]
            cards in hand of the player for the game
        hierarchy: str
            status of the players (president, asshole ...)

    Methods
    -------
    __init__(args)
    add_to_hand(Card)
    remove_from_hand(Card)
    play()
    sorting()
    trouduc_method()
    president_methode()
    """

    def __init__(self, *args):
        if len(args) == 0:
            self.name = names.get_last_name()
            self.hand: list[Card] = []
            self.hierarchy: str = ""
            self.finished: bool = False
        elif len(args) == 1:
            self.name = args[0]
            self.hand: list[Card] = []
            self.hierarchy: str = ""
            self.finished: bool = False
        else:
            self.name = names.get_last_name() if args[0] == '' else args[0]
            self.hand: list[Card] = args[1]
            self.hierarchy: str = ""
            self.finished: bool = False

    def add_to_hand(self, card_to_add: Card):
        """
        method to add one or many cards to hand of the current player

        :param: card_to_add: list of Card
        :return: None
        """
        self.hand.append(card_to_add)

    def remove_from_hand(self, card_to_remove: Card):
        """
        method to remove one or many cards to hand of the current player

        :param: card_to_remove: Card
        :return: None
        """
        self.hand.remove(card_to_remove)

    def play(self):
        """
        method who describe when it's the turn of player to play

        :return: Card
        """
        self.hand = self.sorting()
        print("tour : ", self.name, "\n", self.hand)
        is_played = input(
            "voulez-vous jouer ou vous coucher ? (appuyez sur entré pour jouer, taper 1 pour vous coucher)")
        if is_played == "1":
            self.finished = True
        else:
            card_played = input("quelle(s) cartes voulez-vous jouer ?")
            return Card(card_played[1], card_played[4])


    def sorting(self):
        """
        method to sort the hand of the player using 'bubble sort'

        :return: List of Card sorted
        """
        permutation = True
        passage = 0
        while permutation:
            permutation = False
            passage = passage + 1
            for still_running in range(0, len(self.hand) - passage):
                if self.hand[still_running] > self.hand[still_running + 1]:
                    permutation = True
                    """Exchange 2 elements"""
                    self.hand[still_running], self.hand[still_running + 1] = \
                        self.hand[still_running + 1], self.hand[still_running]
        return self.hand

    def trouduc_method(self):
        """
        method who describe the card to give for the 'trouduc' player

        :return: Card
        """
        card_to_give: Card
        self.hand = self.sorting()
        index = len(self.hand) - 1
        card_to_give = self.hand[index]
        self.remove_from_hand(card_to_give)
        return card_to_give

    def president_method(self):
        """
        method who describe the card to give for the 'president' player

        :return: Card
        """
        card_to_give: Card
        self.hand = self.sorting()
        print(self.hand)
        response = input("quel carte désirez vous donner au trouduc ?")
        card_to_give = Card(response[2], response[7])
        self.remove_from_hand(card_to_give)
        return card_to_give
