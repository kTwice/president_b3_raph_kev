from president_b3_raph_kev.models.PresidentGame import PresidentGame
from president_b3_raph_kev.models.Turn import Turn

'''Main Class of president game app'''
if __name__ == '__main__':
    print("jeux du président")
    number_of_player = input("entrée le nombre de joueur : ")
    game = PresidentGame(int(number_of_player))
    game.gamers_name()
    turn = Turn(int(number_of_player), game.players, True)
    game.end_of_the_game()
    game_continue = input()
    while game_continue != "exit":
        turn2 = Turn(int(number_of_player), game.players, False)
        game.end_of_the_game()
        game_continue = input()

    print("au revoir")

